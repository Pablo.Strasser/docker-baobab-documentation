# What is Docker?
Docker is a software that allows to create software packages called "containers" and "images" which run in isolation of each others.
Docker can be seen as a lightweight version of a virtual machine.
On the other hand Docker can be seen as a more isolated version of a virtual environments like pip or conda.

# What is it used for?
The main advantage of docker is easy deployment and distribution of code as we can simply distribute a docker image which contain all the needed software to run the code. There is a huge library of officially supported images for standard machine learning tools. For example [ngc.nvidia.com](https://ngc.nvidia.com/) contain official Nvidia docker images for pytorch, tensorflow and torch ready to use.

Docker also allows better reproductibility as the same docker image will have exactly the same user space software on all machine it is deployed on.

# How is it different  from a virtual machine or python virtual environment (pip,conda)
Contrary to a virtual machine which simulate a whole computer together with a full operating system including drivers and all. Docker only simulate the userspace portion, using the host kernel and drivers. 
This allows a smaller overhead. We can see docker as using chroot together with namespace feature build into the kernel to provide efficient isolation but still allowing sharing resources.

In practice this mean that processes started inside the container will run as normal process in the host machine only as a different user and on a different file-systems this allows a near zero overhead usage of ressources and no need to indicate the needed ressources like in a virtual machine.

However this also bring the limitation that the host need compatible drivers and kernel. This mean that you cannot launch a windows container/image on a linux machine for example.

On linux theses limitations are summarized into two components, a kernel version and a gpu driver version. Currently the lowest version on the clusters are kernel 2.6 and gpu driver 384.111. This mean that ubuntu 18.04 (bionic) is not usable and ubuntu 16.04 (xenial) should be used as base operating system instead.
For the gpu cuda 9.0 should be used because its the latest cuda version support by the 384 driver.

![ ](Docker_architecture.png  "Docker Architecture comparison. Taken from https://www.docker.com/resources/what-container#/package_software.")

The difference with respect to conda and or pip is that instead of installing everything inside the user home directory and use environment variable to point to the good location. We can install software as root inside the container.

# When should I use docker

- The code use a lot of different libraries.
- Pip or conda is not usable and do not contain the needed library.
- Root access is needed to install software.
- Easy deployment on other machines is needed.

# Docker workflow

The docker workflow can be summarized into three points.

1. Building an image.
2. Deploying an image.
3. Running an image.

## Building an image

The starting point in docker is given by an image which can be seen as the content of a whole file system together with some metadata like default environment variables. The build step consists to create an image by executing command inside a base image. The final goal of this step is to generate a full filesystem with all needed file together with needed environment variables and other metadata. Docker will use caching to avoid to build layers which were already run before, so do not expect the command to be run at each time.

Going more in details an image consist of a succession of layers each of them adding files. This allows to save storage and bandwidth as some standard part may be reused like the operating system or the pytorch installation.

An image is build with a Dockerfile [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/) which can be seen as script giving instructions on how to build the image.

A Dockerfile create an image from a base image extending the base image by adding additional layer on top of it.
Good base image are given by already packaged machine learning algorithm by nvidia [ngc.nvidia.com](https://ngc.nvidia.com/) or full operating systems like ubuntu and alpine which can be found at [Docker hub](https://hub.docker.com/) or even the empty image scratch which is useful for combining different images together.

In the Dockerfile the base image is indicated with the from command.

The main useful command for Dockerfile are:

### RUN
The run command execute the command inside the container. It is used to install software, copy and move file inside the container and execute scripts inside the container. For example:
>RUN apt-get install -y make
>RUN apt-get install -y git
>RUN git clone http://example.com
>WORKDIR example
>RUN make install

Note that each line will create a new layer and will execute in another shell making command like cd useless.

### WORKDIR
Change the working directory for the rests of the commands.

### ENV
Set an environment variable which will be set for the rest of the build process and each time we enter inside a container. It can be seen as equivalent to add the variable in the .bashrc.
An example of use is:
>ENV PATH=/usr/bin

### ARG
Allows to indicate parameter when building the image. Which can then be accessed like a variable ($name).

### COPY
Allows to copy content from the host or another image inside the image we are building.

>COPY . .

### Guideline
There is a document with useful guideline [Guidelines](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#general-guidelines-and-recommendations). The most important guideline is that containers should be ephemeral. This mean that the image need to contain everything that is needed with no additional steps.
This is particulary important on the cluster where container are read only except for mounted home folder.

### Launching a build

An image can be build with the docker build command which will use the file name Dockerfile in the current directory.
>docker build -t name .

### Full example

#### PyTorch with Python 2

This is a minimal example on how to create an PyToyrch image with Python 2.
This image is based on the pip installation from the [official website](https://pytorch.org/). And it will contain exactly the same installation of Pytorch than if you will be using pip.

The creation of an image consists of the following steps:
1. Choose a base image that fill our needs. For this example a cuda image with cudnn fill our need. As this image is on an ubuntu operating system we can install all ubuntu packages with apt-get commands.
2. Install the needed dependencies. In our case it consists of Pip.
3. Install the software we want to install. In this case we simply use pip to install PyTorch as you will on your own PC.

## Deploying an image

An image can be deployed in different manner. It can be stored on a registry like Docker hub. This can be done with the use of the Docker login
Docker push and Docker pull command.

It is also possible to save everything as a tar archive with docker save and load with docker load.

On the different clusters a conversion step need to be done to transform a docker image to their native format.

### Baobab (Singularity)

Baobab use a HPC variant of docker called Singularity. A docker image can be converted to their format.

First we load the singularity module.
>module load GCC/6.3.0-2.27 Singularity/2.4.2

We then export username and password if we use a private repository.
>export SINGULARITY_DOCKER_USERNAME="MY USERNAME"
>export SINGULARITY_DOCKER_PASSWORD="MY PASSWORD"

We then do the conversion.
>singularity build /home/strassp6/scratch/gitlab/$2.simg  docker://$1
Where $1 is the url of the image and $2 the name of the file where the singularity image is stored.

Then we can launch the experiment like that.
>srun -p shared-gpu --gres=gpu:1 singularity exec --nv /home/strassp6/scratch/gitlab/$2.simg bash -c "cd /workspace/OOPML/build; make test"

The srun part is the normal slurm sheduling command. The singularity exec command allows to execute a command inside the container.
The option --nv indicates we want to have gpu support.
Then it is the path to the image we saved before and finally the command we want to execute inside the container.

### CSCS (Shifter)

CSCS use a HPC variant of docker called shifter. A docker image can be converted to their format.

First on your machine export the docker image.
> docker save --output filename.tar image

Copy the tar to the cluster with rsync.

Load the needed module on the cluster.
>module load shifter-ng
>module load daint-gpu

Import the image.
>srun -C gpu -i none shifter load /scratch/snx3000/strasser/$1.tar $1

Execute the image.
>srun -C gpu -i none shifter run --writable-volatile=/workspace load/library/$1 bash -c "cd /workspace/OOPML/build; make test"

## Running an image

A container can be run from an image with the following command:
>docker run -it nameImage command

Where nameImage is the name of the image and command is a command existing inside the container (bash or python3 myscript.py for example).
This command will create a container run the command interactively allowing user inputs and stop the container once the command finish.

Similar command can be used on the cluster (shifter run and singuarity exec ).

# GPU Usage
To use gpu with docker on your own machine you should install [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) and replace all docker command by nvidia-docker or add the default-runtime=nvidia flag to docker starting script.

Its is the recommended to use as base image an nvidia provided image or an image based on a nvidia provided image.

# Tips and Tricks

For prototyping and testing code on your local machine a good trick is to mount your inside code directory to your outside code directory for ease of editing. This can be done by adding the v flags.
>-v Outside_Path:Inside_Path.